from alpine:edge

#find libgit2 and libuv in alpine repos

run apk update \
apk add cmake git\
git clone https://github.com/wren-lang/wren-cli \
make -C wren-cli/projects/make
