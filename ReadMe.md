### Quark ###

This project is intended as a build system/package manager for the Gravity programming language.

I may rewrite this in pure Gravity as the language matures, one benefit of having it in c++ is that it allows for
Quark to install Gravity if it is not found.

# Getting Started: #
Run `./build.gr` to build and compile the project into the `bin/` folder.

Ideas/Plans:
- project file written in Gravity
- self descripted task/functions like in nimble
- git based package manager using libgit2

Goals:
- get a git dependancy list to pull from a Gravity file
- basic commands like build/run working
- allow user defined task

Resources:
- https://libgit2.org/docs/guides/101-samples/
- https://gravity-lang.org/
