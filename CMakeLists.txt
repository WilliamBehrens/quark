################
# Project info #
################
set(CMAKE_C_COMPILER clang)
set(CMAKE_CXX_COMPILER clang++)

include("${CMAKE_CURRENT_LIST_DIR}/cmake/HunterGate.cmake")
HunterGate(
    URL "https://github.com/cpp-pm/hunter/archive/v0.23.297.tar.gz"
    SHA1 "3319fe6a3b08090df7df98dee75134d68e2ef5a3"
)
set(CMAKE_CXX_STANDARD 17)
set(CMAKE_CXX_STANDARD_REQUIRED ON)
set(CMAKE_EXPORT_COMPILE_COMMANDS ON)
set(CMAKE_BUILD_TYPE DEBUG)
set(CMAKE_CXX_FLAGS "-Wall -Wextra") # -Werror
set(CMAKE_MODULE_PATH ${CMAKE_MODULE_PATH} "${CMAKE_SOURCE_DIR}/src/3pc/cmake")

cmake_minimum_required(VERSION 3.0)
set(PROJNAME quark)
project(${PROJNAME})

set(AUTHOR "William Behrens")
set(VERSION "pre-alpha")
set(DESCRIPTION "A build system and dependancy manager for Gravity")

###########
# Options #
###########
option(BUILD_TESTS "Builds test programs" OFF)
option(QUIET "Silences extra build info" OFF)

##############
# Print info #
##############
if(NOT QUIET)
    message("**************************************************")
    message("Build for: ${CMAKE_PROJECT_NAME}")
    message("Author: ${AUTHOR}")
    message("Version: ${VERSION}")
    message("Description: ${DESCRIPTION}")
    message("Build Type: ${CMAKE_BUILD_TYPE}")
    message("Configuration: BUILD_TESTS = ${BUILD_TESTS}")
    message("**************************************************")
endif(NOT QUIET)

#############
# Variables #
#############

#########################
# Dependancies/Packages #
#########################
include("${CMAKE_CURRENT_LIST_DIR}/cmake/Gravity.cmake")
include("${CMAKE_CURRENT_LIST_DIR}/cmake/Findgit2.cmake")

#########################
# Build primary library #
#########################
if(NOT DEFINED WITHOUT_${PROJNAME})
    include(src/${PROJNAME}/CMakeLists.txt)
endif(NOT DEFINED WITHOUT_${PROJNAME})

#if(NOT DEFINED WITHOUT_${PROJNAME}_LIB)
#    include(src/${PROJNAME}_lib/CMakeLists.txt)
#endif(NOT DEFINED WITHOUT_${PROJNAME}_LIB)

if(NOT DEFINED WITHOUT_MYLOG)
    include(src/mylog/CMakeLists.txt)
endif(NOT DEFINED WITHOUT_MYLOG)

###############
# Build tests #
###############
if(BUILD_TESTS)
endif(BUILD_TESTS)

