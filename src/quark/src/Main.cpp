#include "mylog.hpp"
#include <string>


int main(int argc, char* argv[]) {
	for( int i = 1; i <= (argc - 1); i++ ) {
		if( std::string(argv[i]) == "build" ) {
			mylog::note("build");
			break;
		} else if( std::string(argv[i]) == "init" ) {
			mylog::note("init");
			break;
		} else if( std::string(argv[i]) == "run" ) {
			mylog::note("run");
			break;
		} else {
			mylog::note("Unknown command: " + std::string(argv[i]));
		}
	}

	return 0;
}
